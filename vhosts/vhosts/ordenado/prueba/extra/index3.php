<?php

include('dbconfig.php');

?>

<!DOCTYPE HTML>
<html>

<head>

  <title>Bootstrap sortable table columns example</title>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />

  <link rel="stylesheet" href="https://drvic10k.github.io/bootstrap-sortable/Contents/bootstrap-sortable.css" />

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment.js"></script>

  <script src="https://drvic10k.github.io/bootstrap-sortable/Scripts/bootstrap-sortable.js"></script>

  <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

</head>

<body>
    
    <h1 style="color:green;">  
        GeeksforGeeks  
    </h1> 
      
    <p id="geeks" style= 
        "font-size:20px; font-weight:bold"> 
    </p> 
      
    <select id="arr"></select> 
      
    <br><br> 
      
    <button onclick="GFG_Fun();"> 
        click here 
    </button> 
      
    <p id="gfg" style="font-size: 26px; 
        font-weight: bold;color: green;"> 
    </p> 

<script> 


$(document).ready(function(){
	$("#cbx_municipio").change(function () {
		$("#cbx_municipio option:selected").each(function () {
			id_municipio = $(this).val();
				function GFG_Fun() { 
            for (var i = 0; i < elmts.length; i++) { 
                var optn = elmts[i]; 
                var el = document.createElement("option"); 
                el.textContent = optn; 
                el.value = optn; 
                select.appendChild(el); 
            } 
            down.innerHTML = "Elements Added"; 
        }
			
		});
	})
});        
    </script> 

<div class="container">

  <h1>Datos de las interfaces</h1>

  <table class="table table-bordered sortable">

<thead>
	<tr>

        <th>TXB</th>

	<th>TXP</th>

	<th>RXB</th>

	<th>RXP</th>

      </tr>

<thead>

				<?php
                                $first_query = "SELECT `id` , COUNT(*) FROM InterfacesTB2 GROUP BY `id` ORDER BY `date` DESC"; #
				$array_first = $DBcon->prepare($first_query);
				$array_first->execute();

				$userData = array();
				
				$value_1 = 0;

				while($row_first=$array_first->fetch(PDO::FETCH_ASSOC)){
			        $id_row = $row_first['id'];
			        $query = "SELECT `date`, `id`, COUNT(*) FROM InterfacesTB2 WHERE date = (select MAX(`date`) from `InterfacesTB2` WHERE id LIKE '$id_row')";
			        $arrayDB = $DBcon->prepare($query);
			        $arrayDB->execute();
			        $row=$arrayDB->fetch(PDO::FETCH_ASSOC);
			        $date_row = $row['date'];
			        $cantidad = $row['COUNT(*)'];
			        $query2 = "SELECT `TXbytesDB`, `TXpacketsDB`, `RXbytesDB`, `RXpacketsDB`, `date` FROM `InterfacesTB2` WHERE `id` LIKE '$id_row' && date = '$date_row' ORDER BY `InterfacesTB2`.`date` DESC";
			        $arrayDB_2 = $DBcon->prepare($query2);
			        $arrayDB_2->execute();
				$value = $cantidad;
				
#				$userData['Interfaces'] = "Sucursal con ID: $id_row";
				
			        for ( $i=0 ; $i < $cantidad ; $i++ ){
		
			                $row_2=$arrayDB_2->fetch(PDO::FETCH_ASSOC);
					$userData["Datos"] = $row_2;			        
				
				?>
				<tbody>
					<tr>		    
						<td><?php echo $userData["Datos"]["TXbytesDB"];?></td>
						<td><?php echo $userData["Datos"]["TXpacketsDB"];?></td>
                                                <td><?php echo $userData["Datos"]["RXbytesDB"];?></td>
						<td><?php echo $userData["Datos"]["RXpacketsDB"];?></td>
					</tr>
				</tbody>
                                        
				<?php }} ?>	
                        </table>
		</div>
	</body>	
</html>
