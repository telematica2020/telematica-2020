<?php 
$OC_Version = array(15,0,14,1);
$OC_VersionString = '15.0.14';
$OC_Edition = '';
$OC_Channel = 'stable';
$OC_VersionCanBeUpgradedFrom = array (
  'nextcloud' => 
  array (
    '14.0' => true,
    '15.0' => true,
  ),
  'owncloud' => 
  array (
  ),
);
$OC_Build = '2019-12-19T08:25:02+00:00 dd0b38876d11d81776c9e12bdcc31142059a983e';
$vendor = 'nextcloud';
