<?php include("seguridad.php"); ?>

<?php
//$data = file_get_contents("https://feeds.citibikenyc.com/stations/stations.json");
$data = file_get_contents("https://member.citibikenyc.com/stations/stations.json");
$estaciones = json_decode($data, true);

foreach ( $estaciones as $estacion) {        
	
	$r = array_filter( $estacion, function( $e ) {
	
	
		return $e['availableDocks'];
	});
}

for ($i = 0 ; $i <= 957 ; $i++){
	
	$s = $r[$i];
	$varBikes[$i]= $s['availableBikes'] . "<i> bicis </i>disponibles en " . "<b>" . $s['stationName'] . "</b>";
	$varDocks[$i]= $s['availableDocks'] . "<i> docks </i>disponibles en " . "<b>" . $s['stationName'] . "</b>";
}

rsort($varBikes, SORT_NUMERIC);
rsort($varDocks, SORT_NUMERIC);
$varBikes = array_slice($varBikes,0,10);
$varDocks = array_slice($varDocks,0,10);

?>

<!DOCTYPE html>
	<html lang="es">
	<link rel="stylesheet" type="text/css" href="estilotop.css"/>
	<link rel="stylesheet" media="(max-width: 800px)" href="estilotop.css"/>
	<link rel="shortcut icon" href="top.ico" type="image/x-icon"/>
<head>

<title>Ranking</title>
<meta charset="utf-8">
</head>

<body>
<div id="todo" class="ContentForm">
	<div id="title" class="ContentForm">
	<h1>Top 10 <br>Estaciones con más bicicletas y docks disponibles</h1>
	</div>
	<table id="table1" align="left" border="10">

		<tr>	
			<th>Top 10 Docks</th>	
		</tr>	
	
		<?php
			foreach ($varDocks as $Docks){
		?>
		
		<tr>
			<td><?php echo $Docks ?></td>
		</tr>
		
		<?php } ?>
	</table>

	<table id="table2" align="rigth" border="10">
		<tr>
                	<th>Top 10 Bikes</th>
                </tr>

		<?php
			foreach ($varBikes as $Bikes){
		?>
		<tr>
			<td><?php echo $Bikes ?></td>
		</tr>
		<?php } ?>
	</table>
<div>

<a href="virar.php"> <button class="btn btn-lg btn-primary btn-block btn-signin" id="atras" type="submit">Atras</button> </a>
<a href="salir.php"> <button class="btn btn-lg btn-primary btn-block btn-signin" id="salir" type="submit">Salir</button> </a>

</body>
</html>
