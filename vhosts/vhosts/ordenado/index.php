<?php
require ('conexion.php');

$query = "SELECT id, host FROM InterfacesTBM2";
$resultado = $mysqli->query($query);
?>

<html>
    <head>
        <title>Tabla</title>


        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" /> 
	<link rel="stylesheet" type="text/css" href="bootstrap-sortable.css">
 <!--       <link rel="stylesheet" href="https://drvic10k.github.io/bootstrap-sortable/Contents/bootstrap-sortable.css" />  -->
       
<!--                <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
                <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment.js"></script>
        -->
        <!-- <script src="https://drvic10k.github.io/bootstrap-sortable/Scripts/bootstrap-sortable.js"></script> -->

<!--	<script src=sort.js></script> -->

<!--        <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet"> -->
<link rel="stylesheet" type="text/css" href="estilo.css">	

        <script language="javascript">
            $(document).ready(function () {
                $("#cbx_host").change(function () {
                    $('#cbx_name').find('option').remove().end().append('<option value="whatever"></option>').val('whatever');

                    $("#cbx_host option:selected").each(function () {
                        id = $(this).val();
                        $.post("getMunicipio.php", {id: id}, function (data) {
                            $("#cbx_name").html(data);

                        });
                    });
                })
            });

            $(document).ready(function () {
                $("#cbx_name").change(function () {
                    $("#cbx_name option:selected").each(function () {
                        name2 = $(this).val();
                        $.post("getLocalidades.php", {name2: name2, id: id}, function (data) {

                            $("#cbx_name2").html(data);
                        });
                    });
                })
	    });

        </script>
    </head>

    <body>
        <form id="combo" name="combo"  method="POST"> 

            <div><div id="secHost">Seleccionar host</div><select name="cbx_host" id="cbx_host">
                    <option value="0">Seleccionar host</option>
                    <?php while ($row = $resultado->fetch_assoc()) { ?>
                        <option value="<?php echo $row['id']; ?>"><?php echo $row['host']; ?></option>
                    <?php } ?>
                </select></div>

            <br /> 

            <div><div id="secInt">Seleccionar Interfaz</div><select name="cbx_name" id="cbx_name"></select></div>

          <!--  <script src="https://drvic10k.github.io/bootstrap-sortable/Scripts/bootstrap-sortable.js"></script>-->
            <!--  <br /> -->
           <script src="https://drvic10k.github.io/bootstrap-sortable/Scripts/bootstrap-sortable.js"></script> 
	    <div id="cbx_name2" class="container">
 <script language="javascript">
	   $(document).ready(function () {
                $("#cbx_name").change(function () {
                    $("#cbx_name option:selected").each(function () {
                        name2 = $(this).val();
                        $.post("getLocalidades.php", {name2: name2, id: id}, function (data) {

                            $("#cbx_name2").html(data);
                        });
                    });
                })
            });
</script> 
            <h1>Datos de las interfaces</h1>
            <table class="table table-bordered sortable">
            </table>
	   
            </div> 

        </form>
    </body>
</html>

