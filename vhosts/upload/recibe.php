<?php

$protocol = "https"
$server = 10.8.0.2

// Valores y opción para la URL de origen
$remote_path = 'archivos/'; // La ruta base en la que vamos a trabajar en el servidor remoto
$source_file_name = 'archivo.txt'; // El nombre del archivo en el servidor remoto cuyo contenido queremos leer.
$url = $protocol.'//'.$server.'/'.$remote_path.$source_file_name; // URL final del archivo en el servidor remoto.
curl_setopt($curl_handle, CURLOPT_URL, $url); // Establecemos la URL final para el archivo en el servidor remoto en cURL.

curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true); // Permitimos que se transfieran datos desde el servidor remoto al de nuestra aplicación.

$content = curl_exec($curl_handle);
$dest_file = '/media/info/grupo2/archivo_recibido.txt';
$file_handle = fopen($dest_file, "w+");
fputs($file_handle, $content);
fclose($file_handle);
curl_close($curl_handle);

?>
